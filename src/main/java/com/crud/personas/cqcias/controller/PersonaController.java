package com.crud.personas.cqcias.controller;

import com.crud.personas.cqcias.model.Persona;
import com.crud.personas.cqcias.model.ServiceResponse;
import com.crud.personas.cqcias.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Inyeccion de servicios al constructor y validaciones
@RestController
@RequestMapping("api/v1/persona")
@CrossOrigin("*")
public class PersonaController {
    @Autowired
    private IPersonaService iPersonaService;

    @GetMapping("/list")
    public ResponseEntity<List<Persona>> list(){
        var result = iPersonaService.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/listId/{id}")
    public ResponseEntity<Persona> list(@PathVariable int id){
        var result = iPersonaService.findId(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<ServiceResponse>  save(@RequestBody Persona persona){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iPersonaService.save(persona);
        if (result == 1){
            serviceResponse.setMessage("Dato guardado con exito");
        }
        return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<ServiceResponse>  update(@RequestBody Persona persona){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iPersonaService.update(persona);
        if (result == 1){
            serviceResponse.setMessage("Dato editado con exito");
        }
        return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<ServiceResponse>  update(@PathVariable int id){
        ServiceResponse serviceResponse = new ServiceResponse();
        int result = iPersonaService.deleteId(id);
        if (result == 1){
            serviceResponse.setMessage("Dato eliminado con exito");
        }
        return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
    }
}
