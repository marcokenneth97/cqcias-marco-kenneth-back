package com.crud.personas.cqcias.model;

import lombok.Data;

@Data
public class ServiceResponse {
    Boolean success;
    String message;
}
