package com.crud.personas.cqcias.model;

import lombok.Data;

import java.util.Date;

//Modelo de los datos
@Data
public class Persona {

    int id;
    String nombre;
    String primer_apellido;
    String segundo_apellido;
    String telefono;
    String estatus;
    Date fecha_ins;
    Date fecha_upd;

}
