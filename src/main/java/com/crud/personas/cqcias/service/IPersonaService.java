package com.crud.personas.cqcias.service;

import com.crud.personas.cqcias.model.Persona;

import java.util.List;

public interface IPersonaService {

    public List<Persona> findAll();
    public Persona findId(int id);
    public int save(Persona persona);
    public int update(Persona persona);
    public int deleteId(int id);
}
