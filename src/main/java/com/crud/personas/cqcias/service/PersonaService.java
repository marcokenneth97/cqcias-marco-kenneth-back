package com.crud.personas.cqcias.service;

import com.crud.personas.cqcias.model.Persona;
import com.crud.personas.cqcias.repository.IPersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//Logica de consulta de datos los cuales se devuelven al repository
@Service
public class PersonaService implements IPersonaService{

    @Autowired
    private IPersonaRepository iPersonaRepository;

    @Override
    public List<Persona> findAll() {

        List<Persona> list;

        try {
            list = iPersonaRepository.findAll();
        }catch (Exception ex){
            throw ex;
        }

        return list;
    }

    @Override
    public Persona findId(int id) {

        Persona list;

        try {
            list = iPersonaRepository.findId(id);
        }catch (Exception ex){
            throw ex;
        }

        return list;
    }

    @Override
    public int save(Persona persona) {
        int row;

        try {
            row = iPersonaRepository.save(persona);
        }catch (Exception ex){
            throw ex;
        }

        return row;
    }

    @Override
    public int update(Persona persona) {
        int row;

        try {
            row = iPersonaRepository.update(persona);
        }catch (Exception ex){
            throw ex;
        }

        return row;
    }

    @Override
    public int deleteId(int id) {
        int row;

        try {
            row = iPersonaRepository.deleteId(id);
        }catch (Exception ex){
            throw ex;
        }

        return row;
    }
}
