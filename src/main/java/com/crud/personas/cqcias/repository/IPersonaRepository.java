package com.crud.personas.cqcias.repository;

import com.crud.personas.cqcias.model.Persona;

import java.util.List;

public interface IPersonaRepository {

    //Obtener lista de datos
    public List<Persona> findAll();
    //Obtener lista de dato de una persona
    public Persona findId(int id);
    //Crear nuevo registro
    public int save(Persona Personas);
    //Actualizar registro
    public int update(Persona Personas);
    //Eliminar registro
    public int deleteId(int id);
}
