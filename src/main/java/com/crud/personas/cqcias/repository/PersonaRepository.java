package com.crud.personas.cqcias.repository;

import com.crud.personas.cqcias.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

//Paquete encargado de consultar en la base de datos
@Repository
public class PersonaRepository implements IPersonaRepository{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Consulta de datos
    @Override
    public List<Persona> findAll() {
        String SQL= "SELECT * FROM Persona WHERE estatus = 1";
        return jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Persona.class));
    }

    //Consulta de dato por id
    @Override
    public Persona findId(int id) {

        String sql = "SELECT * FROM Persona WHERE id = ?";

        return (Persona) jdbcTemplate.queryForObject(
                sql,
                new Object[]{id},
                new BeanPropertyRowMapper(Persona.class));

    }

    //Eliminar registro o cambiar estatus
    @Override
    public int deleteId(int id) {
        String SQL= "UPDATE Persona SET estatus = 0, fecha_upd = GETDATE() WHERE id = ?";
        return jdbcTemplate.update(SQL, new Object[]{id});
    }

    //Inserter datos
    @Override
    public int save(Persona Personas) {
        String SQL= "INSERT INTO Persona VALUES (?,?,?,?,?,?,?)";
        return jdbcTemplate.update(SQL, new Object[]{
                Personas.getNombre(),
                Personas.getPrimer_apellido(),
                Personas.getSegundo_apellido(),
                Personas.getTelefono(),
                Personas.getEstatus(),
                Personas.getFecha_ins(),
                Personas.getFecha_upd()
        });
    }

    //Actualizar datos
    @Override
    public int update(Persona Personas) {
        String SQL= "UPDATE Persona SET nombre = ?, primer_apellido = ?, segundo_apellido = ?, telefono = ?, fecha_upd = ? WHERE id = ?";
        return jdbcTemplate.update(SQL, new Object[]{
                Personas.getNombre(),
                Personas.getPrimer_apellido(),
                Personas.getSegundo_apellido(),
                Personas.getTelefono(),
                Personas.getFecha_upd(),
                Personas.getId()});
    }


}
